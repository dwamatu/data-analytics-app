<?php

namespace App\Http\Controllers;


use App\LogData;
use App\Utilities\BaseUtilities;
use DB;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Schema;


/**
 * Class BaseController
 * @package App\Http\Controllers
 */
class BaseController extends Controller
{


    //<editor-fold desc="Issue Get Request">
    //
    /**
     * @param Request $request
     * @param $table
     * @param null $id
     * @param null $q
     * @return array|\Illuminate\Support\Collection
     */
    public static function issueGetRequest($payload)
    {
        //initiaize response
        $response = array();
        \Log::info("BaseController::issueGetRequest -> request" . json_encode($payload));

        $pageSize = $payload['pageSize'];
        $offSet = $payload['offSet'];
        $all = $payload['all'];
        $q = $payload['q'];
        $table = $payload['table'];
        $id = $payload['id'];
        $where = $payload['where'];
        $andWhere = $payload['andwhere'];

        \Log::info("BaseController::issueGetRequest -> where   " . ($where));

        $filters = array();
        if ((isset($where)))  {
            $where = true;
            $key ="where";
            $filters = self::formatQueryParams($payload, $key, $filters);
            if (isset($andWhere)){
                $key = 'andwhere';
                $filters = self::formatQueryParams($payload, $key, $filters);
            }
        }


        \Log::info("BaseController::issueGetRequest -> filters   " . (json_encode($filters)));
        if (Schema::hasTable($table)) {
            if ($id != null) {
                $data = self::fetchOne($table, $id);
                if (isset($data['error'])) {
                    $response['errors'] = $data;
                } else {

                    $response['resource'] = $data;
                }
            } else {
                $response = BaseUtilities::fetchList($table, $pageSize, $offSet, $all, $q, $where, $filters);

            }
            return $response;
            //
        } else {
            $response['errors'] = ['status_code' => 404, "error" => "resource $table does not exist"];
        }

        return $response;

    }

    /**
     * @param $resource
     * @param $id
     * @return array
     */
    public static function fetchOne($resource, $id)
    {
        $models = [
            'log_data' => LogData::class,

        ];

        try {
            $data = $models[$resource]::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            $data = ["status_code" => 404, 'error' => "$resource not found"];
        }

        return $data;

    }

    /**
     * Extracts model column to be used with the query parameters
     * @param $param
     * @return bool|string
     */
    private static function extractQueryParams($param)
    {
        $param = substr($param, 0, strpos($param, "="));
        return $param;
    }

    /**
     * Extracts
     * @param $param
     * @return bool|string
     */
    private static function extractWhereParams($param)
    {
        $paramLength = strlen($param);
        $param = substr($param, strpos($param, "=") + 1);
        return $param;
    }

    /** Formats the filters
     * @param Request $request
     * @param $filters
     * @return mixed
     */
    public static function formatQueryParams($payload,$key, $filters)
    {
        $params = explode(",", self::extractWhereParams($payload[$key]));
        $queryParam = self::extractWhereParams($payload[$key]);
        $filters[$queryParam] = $params;
        return $filters;
    }

    //</editor-fold>


}
