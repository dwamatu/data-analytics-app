<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});
//logdata Factoru


$factory->define(App\LogData::class, function (Faker\Generator $faker) {

    return [
        'msisdn' => "2332" . $faker->randomDigit() . $faker->randomNumber(7),
        'service' => $faker->randomElement(['AUTHP', 'CUP', 'FCP', 'FECO', 'FELA']),
        'tat' => $faker->randomNumber(),
        'amount' => $faker->randomNumber(),
        'statusCode' => $faker->randomElement($array = array(200, 251, 359)),
        'statusMessage' => $faker->sentence($nbWords = 6, $variableNbWords = true),
        'affiliate' => $faker->randomElement(["UAT", "EGH", "INT"]),
        'serverIp' => $faker->randomElement([47, 57, 157]),

    ];
});
