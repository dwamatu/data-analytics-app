<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AffiliateController extends Controller
{
    protected $response = array();

    /**
     * @param Request $request
     * @param $affiliate
     * @param null $id
     * @param null $q
     * @return array
     */
    public function fetchAffiliateRequests(Request $request, $affiliate)
    {
        $table = "log_data";
        $pageSize = $request->query('pageSize');
        $offSet = $request->query('offSet');
        $all = $request->query('all');
        $q = $request->query('q');
        $where = $request->query("where");
        $andWhere = $request->query("andwhere");

        $requestPayload = array(
            "table" => $table,
            "pageSize" => $pageSize,
            "offSet" => $offSet,
            "all" => $all,
            "q" => $q,
            "id" => null,
            "affiliate" => $affiliate,
            "where" => $where,
            "andwhere" => $andWhere,
        );
        \Log::info("Request parameters". json_encode($requestPayload));

        $this->response= AffiliateUtilities::fetchAffiliateData($requestPayload);

        $this->response =  BaseController::issueGetRequest($requestPayload);


        return $this->response;

    }
}
