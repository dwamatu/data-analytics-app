<?php
/**
 * Created by PhpStorm.
 * User: black
 * Date: 22/02/2018
 * Time: 17:15
 */

namespace App\Utilities;

use DB;


class BaseUtilities
{
    public static function fetchList($table, $pageSize = null, $offset = null, $all = false, $q = null, $where = false, $filters = null, $andWhere = null)
    {

        $responseCollection = collect([]);

        $iTotal = DB::table($table)->count();

        try {

            if ((isset($all)) && (!isset($where))) {

                $results = DB::table($table)->get();
            } else if ($where && !empty($filters)) {
                //Collection to hold everything
                $queryCollection = collect($filters);
                \Log::info("BaseUtilities::Fetchlist:: filters" . json_encode($filters));
                $query = DB::table($table);
                foreach ($queryCollection->keys() as $key) {
                    $item = $queryCollection->get($key);
                    if (is_array($item)) {
                        $i = 0;
                        foreach ($item as $subitem) {
                            if ($i == 0) {
                                $query->where($key, 'like', $subitem);
                            } else {
                                $query->orWhere($key, 'like', $subitem);
                            }
                            $i++;
                        }
                    } else {
                        $query->where($key, $item);
                    }
                }
                $iTotal = count($query->get());
                if (isset($all)) {
                    $results = collect($query->get());
                } else if (isset($pageSize) && !empty($offset)) {
                    $results = collect($query->get())->forPage($offset, $pageSize);
                } else {
                    $results = collect($query->get())->forPage(0, 10);
                }


            } else if (isset($pageSize) && !empty($offset)) {

                $results = DB::table($table)->skip($offset)->take($pageSize)->get();
            } else if (isset($q)) {
                $results = DB::table($table)->where()->get();
            } else {
                $results = DB::table($table)->skip(0)->take(10)->get();

            }

            $iFilteredTotal = count($results);


            $responseCollection->put('iTotal', $iTotal);
            $responseCollection->put('iFilteredTotal', $iFilteredTotal);
            $responseCollection->put('results', $results);

            \Log::info(' iTotal ' . $iTotal . ' iFilteredTotal '.$iFilteredTotal);


        } catch (\PDOException $pdoException) {

            $responseCollection->put('errors', array(['status_code' => 500, "status_message" => "Oops there's something wrong with your request"]));
        }


        return $responseCollection;

    }

}