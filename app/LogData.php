<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogData extends Model
{
    protected $table = 'log_data';
    protected $primaryKey = 'logID';

    protected $fillable = [ 'msisdn','serviceCode','tat','amount','statusCode','statusMessage','affiliate','serverIp' ];
}


